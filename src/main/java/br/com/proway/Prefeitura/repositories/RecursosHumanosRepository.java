package br.com.proway.Prefeitura.repositories;

import br.com.proway.Prefeitura.models.RH;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;

import java.util.ArrayList;

public interface RecursosHumanosRepository extends JpaRepository<RH, Long> {
    @NonNull ArrayList<RH> findAll();
    @NonNull RH save(@NonNull RH recursosHumanos);
}

package br.com.proway.Prefeitura.models;

import jakarta.persistence.*;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Data
@Entity
@Table(name = "recursos_humanos")
@EntityListeners(AuditingEntityListener.class)
public class RH {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "funcionario")
    private String funcionario;

    @Column(name = "salario")
    private Double salario;
}

package br.com.proway.Prefeitura.controllers;

import br.com.proway.Prefeitura.models.RH;
import br.com.proway.Prefeitura.repositories.RecursosHumanosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/recursos-humanos")
public class RHController {

    @Autowired
    RecursosHumanosRepository repository;

    @GetMapping("")
    public ArrayList<RH> getAll() {
        return repository.findAll();
    }

    @PostMapping("")
    public ResponseEntity post(@RequestBody RH recursosHumanos) {
        RH novoRH = new RH();

        try {
            novoRH = repository.save(recursosHumanos);

            return new ResponseEntity<RH>(novoRH, HttpStatus.CREATED);
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());

            return new ResponseEntity<String>( "[ERRO]: \n" + e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
